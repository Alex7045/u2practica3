package com.example.u2_practica2;

import android.os.Bundle;

public class NextActivity extends LogActivity {
    public NextActivity() {
        super.setDEBUG_TAG(this.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
    }
}